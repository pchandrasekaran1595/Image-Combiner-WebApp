import io
import cv2
import base64
import numpy as np

from PIL import Image

#####################################################################################################

class ImageCodecs(object):

    def __init__(self):
        pass

    def decode_image(self, imageData) -> tuple:
        header, imageData = imageData.split(",")[0], imageData.split(",")[1]
        image = np.array(Image.open(io.BytesIO(base64.b64decode(imageData))))
        image = cv2.cvtColor(src=image, code=cv2.COLOR_BGRA2RGB)
        return header, image


    def encode_image(self, header: str, image: np.ndarray) -> str:
        _, imageData = cv2.imencode(".jpeg", image)
        imageData = base64.b64encode(imageData)
        imageData = str(imageData).replace("b'", "").replace("'", "")
        imageData = header + "," + imageData
        return imageData

image_codecs = ImageCodecs()    

####################################################################################################

def combine(image_1: np.ndarray, image_2: np.ndarray, vertical: bool, adapt_small: bool):
    h1, w1, _ = image_1.shape
    h2, w2, _ = image_2.shape

    if vertical:
        if w1 > w2:
            if adapt_small:
                image_2 = cv2.resize(src=image_2, dsize=(w1, h2), interpolation=cv2.INTER_AREA)
            else:
                image_1 = cv2.resize(src=image_1, dsize=(w2, h1), interpolation=cv2.INTER_AREA)

        elif w2 > w1:
            if adapt_small:
                image_1 = cv2.resize(src=image_1, dsize=(w2, h1), interpolation=cv2.INTER_AREA)
            else:
                image_2 = cv2.resize(src=image_2, dsize=(w1, h2), interpolation=cv2.INTER_AREA)
                
        return np.vstack((image_1, image_2))
    
    else:
        if h1 > h2:
            if adapt_small:
                image_2 = cv2.resize(src=image_2, dsize=(w2, h1), interpolation=cv2.INTER_AREA)
            else:
                image_1 = cv2.resize(src=image_1, dsize=(w1, h2), interpolation=cv2.INTER_AREA)

        elif h2 > h1:
            if adapt_small:
                image_1 = cv2.resize(src=image_1, dsize=(w1, h2), interpolation=cv2.INTER_AREA)
            else:
                image_2 = cv2.resize(src=image_2, dsize=(w2, h1), interpolation=cv2.INTER_AREA)
                
        return np.hstack((image_1, image_2))

#####################################################################################################
