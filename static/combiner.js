main = () => {

    let image_input_1 = document.querySelector("#image_input_1")
    let image_input_2 = document.querySelector("#image_input_2")

    let hidden_canvas_1 = document.querySelector("#image_canvas_1")
    let hidden_ctx_1 = hidden_canvas_1.getContext("2d")
    let hidden_canvas_data_1 = null

    let hidden_canvas_2 = document.querySelector("#image_canvas_2")
    let hidden_ctx_2 = hidden_canvas_2.getContext("2d")
    let hidden_canvas_data_2 = null

    let result_canvas = document.querySelector("#result_canvas")
    let result_ctx = result_canvas.getContext("2d")

    let preview_canvas = document.querySelector("#preview_canvas")
    let preview_ctx = preview_canvas.getContext("2d")
    let preview_w = preview_canvas.getAttribute("width")
    let preview_h = preview_canvas.getAttribute("height")

    let vertical_element = document.querySelector("#vertical")
    let adapt_big_element = document.querySelector("#adapt_big")

    let combine = document.querySelector("#combine")
    let reset = document.querySelector("#reset")

    let link_section = document.querySelector("#link_section")
    let link = document.querySelector("a")

    let vertical = 0
    let adapt_small = 0
    let hidden_image_1 = new Image()
    let hidden_image_2 = new Image()
    let preview_image = new Image()
    let result_image = new Image()


    vertical_element.addEventListener("change", () => {
        if(vertical_element.checked){
            vertical = 1
        }
        else{
            vertical = 0
        }
    })

    adapt_big_element.addEventListener("change", () => {
        if (adapt_big_element.checked){
            adapt_small = 1
        }
        else{
            adapt_small = 0
        }
    })

    image_input_1.addEventListener("change", (e1) => {
        if(e1.target.files){
            let imageFile = e1.target.files[0]
            let reader = new FileReader()
            reader.readAsDataURL(imageFile)
            reader.onload = (e2) => {
                hidden_image_1.src = e2.target.result

                hidden_image_1.onload = () => {
                    hidden_canvas_1.setAttribute("width", hidden_image_1.width)
                    hidden_canvas_1.setAttribute("height", hidden_image_1.height)

                    hidden_ctx_1.drawImage(hidden_image_1, 0, 0, hidden_canvas_1.width, hidden_canvas_1.height)
                    hidden_canvas_data_1 = hidden_canvas_1.toDataURL("image/jpeg", 0.92)
                }
            }
        }
    })

    image_input_2.addEventListener("change", (e1) => {
        if(e1.target.files){
            let imageFile = e1.target.files[0]
            let reader = new FileReader()
            reader.readAsDataURL(imageFile)
            reader.onload = (e2) => {
                hidden_image_2.src = e2.target.result

                hidden_image_2.onload = () => {
                    hidden_canvas_2.setAttribute("width", hidden_image_2.width)
                    hidden_canvas_2.setAttribute("height", hidden_image_2.height)

                    hidden_ctx_2.drawImage(hidden_image_2, 0, 0, hidden_canvas_2.width, hidden_canvas_2.height)
                    hidden_canvas_data_2 = hidden_canvas_2.toDataURL("image/jpeg", 0.92)
                }
            }
        }
    })

    combine.addEventListener("click", () => {
        if (hidden_canvas_data_1 === null || hidden_canvas_data_2 === null){
            alert("Please upload two images before combininig")
        }
        else{

            let data = {
                data : JSON.stringify({
                    imageData_1 : hidden_canvas_data_1,
                    imageData_2 : hidden_canvas_data_2,
                    vertical : vertical,
                    adapt_small : adapt_small,
                })
            }

            $.ajax({
                type : "POST",
                url : "",
                headers : {
                    "X-CSRFToken" : Cookies.get("csrftoken"),
                },
                data : data,
                success : (response) => {
                    console.log("----------")
                    console.log(`Success, StatusText : ${response["statusText"]}`)
                    console.log("----------")
                    
                    result_image.src = response["imageData"]
                    result_image.onload = () => {
                        preview_image.src = response["imageData"]
                        preview_ctx.drawImage(preview_image, 0, 0, preview_w, preview_h)
                        result_canvas.setAttribute("width", result_image.width)
                        result_canvas.setAttribute("height", result_image.height)
                        result_ctx.drawImage(result_image, 0, 0, result_canvas.width, result_canvas.height)

                        link.setAttribute("target", "_blank")
                        link.setAttribute("download", "image.jpg")
                        link.setAttribute("href", result_canvas.toDataURL("image/jpeg", 0.92))
                        link_section.hidden = false
                    }
                },
                error : (response) => {
                    console.log("----------")
                    console.log(`Failure, StatusText : ${response["statusText"]}`)
                    console.log("----------")
                },
            })
        }
    })

    reset.addEventListener("click", () => {
        image_input_1.value = ""
        image_input_2.value = ""
        preview_ctx.clearRect(0, 0, preview_w, preview_h)
        preview_image.src = ""
        hidden_image_1.src = ""
        hidden_image_2.src = ""
        hidden_canvas_data_1 = null
        hidden_canvas_data_2 = null
        result_image.src = ""
        vertical_element.checked = false
        adapt_big_element.checked = false
        vertical = 0
        adapt_small = 0
        link_section.hidden = true
    })

}

main()