import json

from django.shortcuts import render
from django.http import JsonResponse

from static.utils import image_codecs, combine

def index(request):
    if request.method == "POST":
        JSONData = request.POST.get("data")

        imageData_1 = json.loads(JSONData)["imageData_1"]
        imageData_2 = json.loads(JSONData)["imageData_2"]
        vertical = int(json.loads(JSONData)["vertical"])
        adapt_small = int(json.loads(JSONData)["adapt_small"])

        if vertical != 0: 
            vertical = True
        else:
            vertical = False

        if adapt_small != 0: 
            adapt_small = False
        else:
            adapt_small = True

        header_1, image_1 = image_codecs.decode_image(imageData_1)
        header_2, image_2 = image_codecs.decode_image(imageData_2)

        image = combine(image_1, image_2, vertical, adapt_small)

        encoded_image = image_codecs.encode_image(header_1, image)

        return JsonResponse({
            "imageData" : encoded_image,
        })

    return render(request=request, template_name="combiner/index.html", context={})
